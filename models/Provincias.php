<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Provincias".
 *
 * @property string|null $autonomia
 * @property string|null $provincia
 * @property int|null $poblacion
 * @property int|null $superficie
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poblacion', 'superficie'], 'integer'],
            [['autonomia', 'provincia'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autonomia' => 'Autonomia',
            'provincia' => 'Provincia',
            'poblacion' => 'Poblacion',
            'superficie' => 'Superficie',
        ];
    }
}
